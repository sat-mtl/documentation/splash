User interface
==============

This section covers topics on the user interface, starting with some information about the graphical user interface, or GUI.

--------------

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: In this section:

   *

--------------

Graphical user interface
------------------------

The GUI appears in the very first Window created (this means that if you prefer having the GUI in its own window, you can specify the first window as empty, i.e. no link to anything, this can be very handy). Once Splash is launched, the GUI appears by pressing Ctrl + Tab. The Splash Control Panel is divided in tabulations, as follows:

.. figure:: ../../_static/images/gui.jpg
   :alt: Splash control panel

   Splash control panel

-  Main: contains very generic commands, like computing the blending map
-  Graph: shows the scene graph and allows for controlling the
   parameters of every object,
-  Medias: allows for changing input files,
-  Filters: shows all image filters, useful when multiple filters are
   used in cascade on a single input media,
-  Meshes: allows for changing 3D models,
-  Cameras: shows a global view of the projection surface, which can be
   switched to any of the configured Cameras. This tabulation is also
   used to set geometric calibration up,
-  Warps: this is where projection warping is modified, in these cases
   where the projection surface is not perfectly reproduced by the 3D
   model.

At the bottom of the GUI you will find performance counters, the logs as
well as the current master clock.



