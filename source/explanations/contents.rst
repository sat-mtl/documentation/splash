Explanations
============

.. toctree::
   :maxdepth: 1
   :glob:
   :caption: In this section:

   *
   ui/contents

This section contains explanations about the choices that were made for this project, as well as past materials explaining the project.

The :doc:technical information page <technical_information>` contains a discussion on the software architecture, while the :doc:`user interface page <ui/contents>` explains the graphical user interface of Splash.

