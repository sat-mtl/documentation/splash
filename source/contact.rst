Contact
=======

This free, libre open source software was made by the Metalab team.

To know more about us, visit `Metalab <https://sat.qc.ca/fr/recherche/metalab>`__.

This project is made possible thanks to the `Society for Arts and Technologies <http://www.sat.qc.ca>`__ (also known as SAT).


Email
-----

Get in touch by email : `metalab@sat.qc.ca <mailto:metalab@sat.qc.ca>`__.
