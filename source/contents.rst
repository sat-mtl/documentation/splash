Getting started with Splash
===========================

You will find all the information necessary to help you get started with
Splash in the following links:

-  :doc:`how to install Splash <install/contents>`
-  :doc:`first steps with Splash <tutorials/first_steps>`
-  :doc:`information on the GUI interface <explanations/ui/contents>`

.. figure:: _static/images/splash_sato.jpg
   :alt: Splash in a 20 meters fulldome

   Splash in a 20 meters fulldome
   
.. toctree::
   :maxdepth: 1
   :caption: On this site:
   
   install/contents
   tutorials/contents
   reference/contents
   howto/contents
   explanations/contents
   faq/contents
   community
   contact

   Previous version <https://sat-metalab.gitlab.io/splash/mkdocs/index.html>
   
   Back to main page <index>
   Back to Metalab main page <https://sat-metalab.gitlab.io>

