Guides for advanced usages
==========================

This section provides information about more advanced usages of Splash,
which go beyond a simple installation of a computer connected to a few
videoprojectors and reading a video file or an input from a capture
card.

.. toctree::
   :maxdepth: 1
   :glob:
   :caption: In this section:
   
   python_scripting
   smpte_master_clock
   set_output_audio_device
   glsl_filter_shaders
   piping_video
   grabbing_rendered_images
   ndi_network_streams
   digital_camera

