Install through Metalab's MPA
=============================

Splash is packaged for the latest long term support (LTS) version of Ubuntu, and is located in Metalab's package archive (MPA).

Metalab's MPAs are listed in `the distribution subgroup of SAT's Gitlab group <https://gitlab.com/sat-mtl/distribution>`__.

Install the MPA
---------------

Note that the correct MPA depends on your architecture and distribution.

Please refer to the repository names to get a glimpse at which architecture/distribution combination we support.

For example to install on Ubuntu 22.04, the documentation is located `here <https://gitlab.com/sat-mtl/distribution/mpa-jammy-amd64-nvidia>`__.

Install Splash
--------------

The MPA contains the Splash package as well as some dependencies, among which the shmdata library.

You can install Splash either from the Software manager, or through the command line:

.. code:: bash

   sudo apt install splash-mapper

Uninstall Splash
----------------

The process to uninstall Splash is the same as any other software installed on Ubuntu.

It can be done from the Software manager, or from the command line:

.. code:: bash

   sudo apt remove splash-mapper
