Flatpak installation
====================

This is the recommended installation for Splash.

Installation
------------

Start by installing the `Flatpak archive <https://flatpak.org>`__ itself by executing this command in a Terminal window:

.. code:: bash

   sudo apt install flatpak
   flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

Next, install the Splash Flatpak archive with:

.. code:: bash

   flatpak install com.gitlab.sat_metalab.Splash

Splash should now be available from your application menu - this may
require to logout and login again. 

It is possible to run the Flatpak version of Splash from the command line:

.. code:: bash

   flatpak run com.gitlab.sat_metalab.Splash
   

Two windows will open and the Splash logo will be available in the Dock.

You can start using Splash.

Uninstallation
--------------

To uninstall the Flatpak package:

.. code:: bash

    flatpak uninstall com.gitlab.sat_metalab.Splash


More information
----------------

Here is the link for the `Flatpak Splash
package <https://flathub.org/apps/details/com.gitlab.sat_metalab.Splash>`__.

A known limitation of the Flatpak package is that it has no access to Jack, and cannot use multiple GPUs.

The Flatpak archive is compatible with most Linux distributions.
