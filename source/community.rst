Community
=========

Get in touch with us by `asking questions on the issue tracker <https://gitlab.com/sat-metalab/splash/issues>`__ or through channel #splash on `Libera.chat IRC chatroom <https://web.libera.chat>`__.
